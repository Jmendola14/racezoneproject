#include "MyGameInstance.h"
#include "OnlineSessionSettings.h"

 struct playerGuy {
	int playerID;
	int playerPlace;
	int currentLap;
	int lapID;
};

TArray<playerGuy> placements;

UMyGameInstance::UMyGameInstance(const FObjectInitializer& ObjectInitializer)
{
	
}
IOnlineSubsystem* SubSystem;
IOnlineSessionPtr SessionInterface;
void UMyGameInstance::Init()
{
	SubSystem = IOnlineSubsystem::Get();
	SessionInterface = SubSystem->GetSessionInterface();
	if (SessionInterface.IsValid())
	{

	}
}
void UMyGameInstance::CreateSession()
{
	FOnlineSessionSettings SessionSettings;
	SessionSettings.bIsLANMatch = true;
	SessionSettings.bShouldAdvertise = true;
	SessionSettings.NumPublicConnections = 2;
}
void UMyGameInstance::DestroySession()
{
	SessionInterface->DestroySession("");
	
//	SessionInterface->OnDestroySessionCompleteDelegates.AddUObject(this, &ClassName::OnDestroySessionComplete);

}
void UMyGameInstance::addMeToInstance(int playerID)
{
	playerGuy aux;
	aux.playerID = playerID;
	aux.playerPlace = 1;
	aux.lapID = 0;
	aux.currentLap = 0;
	placements.Add(aux);

}


void UMyGameInstance::CheckPlacements(int playerID, int currentLap, int lapID)
{
	int placementAux = 1;
	playerGuy aux;
	UE_LOG(LogTemp, Warning, TEXT("checking this ID %d"), playerID);

	for (playerGuy plr : placements)
	{
		if (playerID == plr.playerID)
		{
			plr.currentLap = currentLap;
			plr.lapID = lapID;
			aux = plr;
		}
	}

	for (playerGuy plr : placements)
	{
		if (playerID != plr.playerID)
		{
			if (lapID <= plr.lapID)
				placementAux++;

		}
	}

	for (playerGuy plr : placements)
	{
		if (playerID == plr.playerID)
		{
			plr.playerID = placementAux;
		}
	}


	//aux.playerPlace = placementAux;
	

}

int UMyGameInstance::ReturnPlacement(int playerID)
{

	for (playerGuy plr : placements)
	{

		if (playerID == plr.playerID)
		{
			return plr.playerPlace;
		}
	}
	return 0;
}

void UMyGameInstance::OnDestroySessionComplete(FName SessionName, bool Success)
{
	if (Success)
	{
		CreateSession();
	}
}



void UMyGameInstance::StartSession()
{
	if (SessionInterface.IsValid())
	{
		SessionInterface->StartSession("");
	}

//	SessionInterface->OnStartSessionCompleteDelegates.AddUObject(this, &ClassName::OnStartSessionComplete);

}



void UMyGameInstance::CheckExistingSessions()
{
	FNamedOnlineSession* ExistingSession = SessionInterface->GetNamedSession("");
	if (ExistingSession != nullptr)
	{
		DestroySession();
	}
	else
	{
		CreateSession();
	}
}
