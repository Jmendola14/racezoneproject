// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "BallPlayer.h"
#include "OnlineSubsystem.h"
#include "OnlineSessionInterface.h"

#include "MyGameInstance.generated.h"


/**
 * 
 */
UCLASS()
class ZOMBIEGAME_API UMyGameInstance : public UGameInstance
{
	GENERATED_BODY()

		UMyGameInstance(const FObjectInitializer& ObjectInitializer);

	virtual void Init() override;
	public:

	void CreateSession();
	void CheckExistingSessions();
	void DestroySession();
	void OnDestroySessionComplete(FName SessionName, bool Success);
	void StartSession();
	
	void CheckPlacements(int playerID, int currentLap, int lapID);
	int ReturnPlacement(int playerID);
	void addMeToInstance(int playerID);
	
	TArray<ABallPlayer> players;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "GamePlay")
	int numPlayers=0;
	int maxPlayers;

	
	
};
