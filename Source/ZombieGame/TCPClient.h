// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "HAL/RunnableThread.h"
#include "SocketSubsystem.h"

class ZOMBIEGAME_API TCPClient : public FRunnable
{
public:
	TCPClient(class AMyLevelScriptActor* gLevel);
	~TCPClient();
	virtual bool Init();
	virtual uint32 Run();
	virtual void Stop();
	void HostNewGame(FString sname, FString sport);
	bool IsConnected();
	void RequestServerList();

private:
	FRunnableThread* Thread;
	FSocket* Socket;
	FSocket* ListenerSocket;
	bool running;
	bool connected;
	class AMyLevelScriptActor* GameLevel;

};
