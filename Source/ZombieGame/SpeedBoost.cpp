// Fill out your copyright notice in the Description page of Project Settings.

#include "SpeedBoost.h"
#include "BallPlayer.h"

// Sets default values
ASpeedBoost::ASpeedBoost()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASpeedBoost::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASpeedBoost::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}
void ASpeedBoost::NotifyActorBeginOverlap(AActor* OtherActor)
{
	ABallPlayer* other = dynamic_cast<ABallPlayer*>(OtherActor);
	if (other != nullptr)
	{
		other->ItemID=2;
	}

}

