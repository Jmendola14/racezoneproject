// Fill out your copyright notice in the Description page of Project Settings.


#include "TCPClient.h"
#include "Sockets.h"
#include "MyLevelScriptActor.h"
#include "Interfaces/IPv4/IPv4Address.h"

TCPClient::TCPClient(AMyLevelScriptActor* gLevel)
{
	Thread = FRunnableThread::Create(this, TEXT("TCPClientThread"), 0, TPri_Normal);
	GameLevel = gLevel;
}

TCPClient::~TCPClient()
{
	Stop();
	running = false;
}

void TCPClient::Stop()
{
	running = false;

}

bool TCPClient::IsConnected() {
	return connected;
}

bool TCPClient::Init()
{
	UE_LOG(LogTemp, Warning, TEXT("Message sent!!"));
	Socket = ISocketSubsystem::Get(PLATFORM_SOCKETSUBSYSTEM)->
		CreateSocket(NAME_Stream, TEXT("Default"), false);
	int NewSize = 0;
	Socket->SetReceiveBufferSize(1024, NewSize);
	//put your own ip address
	FIPv4Address MatchMakingServerIP(192, 168, 56, 1);
	TSharedRef<FInternetAddr> MatchMakingServer = ISocketSubsystem::
		Get(PLATFORM_SOCKETSUBSYSTEM)->CreateInternetAddr();
	MatchMakingServer->SetIp(MatchMakingServerIP.Value);
	MatchMakingServer->SetPort(8804);

	connected = Socket->Connect(*MatchMakingServer);

	if (connected)
	{
		UE_LOG(LogTemp, Warning, TEXT("CONNECTED!!"));
		FString message = TEXT("g|#");
		TCHAR* convertedMessage = message.GetCharArray().GetData();
		int32 size = FCString::Strlen(convertedMessage);
		int32 sent = 0;
		bool success = Socket->Send((uint8*)TCHAR_TO_UTF8(convertedMessage), size, sent);
		if (success)
		{
			UE_LOG(LogTemp, Warning, TEXT("Message sent!!"));
			return true;
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("Message not sent!!"));
			return false;
		}
	}
	return false;
}

uint32 TCPClient::Run()
{
	running = true;
	TArray<uint8> RecieveData;
	while (running)
	{
		uint32 size = 0;
		if (Socket->HasPendingData(size))
		{
			RecieveData.Init(0, size);
			int32 readsize = 0;
			Socket->Recv(RecieveData.GetData(), RecieveData.Num(), readsize);
			FString ServerMessage = FString(UTF8_TO_TCHAR(RecieveData.GetData()));

			UE_LOG(LogTemp, Warning, TEXT("Recieved: %s"), *ServerMessage);
			if (ServerMessage[0] == 's')
			{
				GameLevel->UpdateSessionList(ServerMessage);
			}
			else if (ServerMessage[0] == 'o')
			{
				running = false;
				TArray<FString> Out;
				ServerMessage.ParseIntoArray(Out, TEXT("|"), true);
				GameLevel->StartGameHost(FCString::Atoi(*Out[1]));
			}
		}
	}
	return 0;
}

void TCPClient::HostNewGame(FString sname, FString sport) {
	bool canBind = false;

	TSharedRef<FInternetAddr> localIp = ISocketSubsystem::Get(
		PLATFORM_SOCKETSUBSYSTEM)->GetLocalHostAddr(*GLog, canBind);

	if (localIp->IsValid()) {
		FString serialized = "h|" + sname + "|" + localIp->
			ToString(false) + "|" + sport + "|#";
		TCHAR* serializedChar = serialized.GetCharArray().GetData();
		int32 size = FCString::Strlen(serializedChar);
		int32 sent = 0;
		bool successful = Socket->Send((uint8*)TCHAR_TO_UTF8(
			serializedChar), size, sent);
		if (successful)
		{
			UE_LOG(LogTemp, Log, TEXT("MESSAGE SENT!!!!"));
		}
	}
}

void TCPClient::RequestServerList()
{
	FString message = TEXT("g|#");
	TCHAR* ConvertedData = message.GetCharArray().GetData();
	int32 size = FCString::Strlen(ConvertedData);
	int32 sent = 0;
	bool successful = Socket->Send((uint8*)TCHAR_TO_UTF8(
		ConvertedData), size, sent);
	if (successful)
	{
		UE_LOG(LogTemp, Log, TEXT("REQUEST SERVER SENT!!!!"));
	}
}




