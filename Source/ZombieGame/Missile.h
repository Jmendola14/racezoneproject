// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BallPlayer.h"
#include "Missile.generated.h"

UCLASS()
class ZOMBIEGAME_API AMissile : public AActor
{
	GENERATED_BODY()

private:


	bool hasTargetPosition;
	bool hasNoTarget;
	bool wasPick;
	class AActor* target;

	float delayTimer;
	bool hasFinishedDelay;


	float lifetimeCountdown;
	bool canBeDestroyed;
	void Explode();


	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* overlappedComp, AActor* otherActor, UPrimitiveComponent* otherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& hitResult);

public:


	AMissile(const FObjectInitializer& ObjectInitializer);

	//This is target u jesus peace of doodoo
	UPROPERTY(EditAnywhere, Category = "Gameplay")
		ABallPlayer* trgt;

protected:


	virtual void BeginPlay() override;


	//UPROPERTY()
		//class ABallPlayer* PlayerInWorld;

public:


	virtual void Tick(float DeltaTime) override;


	void DelayLogic(float dTime);

	//void FireInDirection(const FVector& ShootDirection);


	void FindPlayer();
	void UpdateTarget();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Collision")
		class UBoxComponent* CollisionComp;


	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Mesh")
		class UStaticMeshComponent* MissileMesh;

	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Movement")
		class UProjectileMovementComponent* ProjectileMovement;


	UPROPERTY(EditDefaultsOnly, Category = "FX")
		class UParticleSystem* ExplosionSystem;

	class UParticleSystemComponent* PlayExplosion(class UParticleSystem* explosion);


	UPROPERTY(EditDefaultsOnly, Category = "Sound")
		class USoundCue* ExplosionSound;

	class UAudioComponent* PlayExplosionSound(class USoundCue* sound);

};
