// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "Math/Rotator.h"
#include "Avatar.h"
#include "MyGameInstance.h"
#include "BallPlayer.generated.h"



class Avatar;
UCLASS()
class ZOMBIEGAME_API ABallPlayer : public APawn
{
	GENERATED_BODY()
private:
	// Timer Handle for Repeating Missile Fire
	FTimerHandle MissileHandle;

public:
	// Sets default values for this pawn's properties
	ABallPlayer();
	UPROPERTY(EditAnywhere, Category = "Gameplay")
		int currentLapID = 0;
	UPROPERTY(EditAnywhere, Category = "Gameplay")
		int currentLap = 0;
	UPROPERTY(EditAnywhere, Category = "Gameplay")
		int currentPlace = 1;
	UPROPERTY(EditAnywhere, Category = "Gameplay")
		int currentID = 0;
	UPROPERTY(EditAnywhere, Category= "Gameplay")
	float timer = 0;
	UPROPERTY(EditAnywhere, Category = "Gameplay")
		float jumpTimer = 0;
	UPROPERTY(EditAnywhere, Category = "Gameplay")
		AAvatar *thisAvatar;

	UPROPERTY(EditAnywhere)


	TSubclassOf<class AAvatar> ToSpawn;
	UPROPERTY(EditAnywhere, BluePrintReadWrite)
		bool Jumped = false;
	UPROPERTY(EditAnywhere)
		TSubclassOf<class AMissile> ToShoot;
	UPROPERTY(EditAnywhere, Category = "Gameplay")
		int ItemID = 0;
	//FVector VelocityHelper;
	UFUNCTION(BlueprintPure, Category = "Timer")
	float GetTimer();
	
	UFUNCTION(BlueprintPure, Category = "Won")
	bool Won();
	UFUNCTION(BlueprintPure, Category = "GetLaps")
		int GetLaps();
	UFUNCTION(BlueprintPure, Category = "GetLaps")
		int GetPlacement();
	UFUNCTION(BlueprintPure, Category = "GetJumpDelay")
		float GetJumpDelay();
	UFUNCTION(BlueprintPure, Category = "SetJumpDelay")
		float SetJumpDelay();
	UFUNCTION(BlueprintPure, Category = "Reset")
		bool ResetEverything();
	UFUNCTION()
		void OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);
//	UFUNCTION()
	//	void NotifyHit(class UPrimitiveComponent* MyComp, AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved,FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit);


	//UFUNCTION()
		//void ReceiveHit(UPrimitiveComponent* MyComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	void Spawn();
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	UPROPERTY(EditDefaultsOnly, Category = "Projectiles")
		TSubclassOf<class AMissile> MissileClass;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* Mesh;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* RootMesh;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	USpringArmComponent* SpringArm;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UCameraComponent* Camera;

	UPROPERTY(EditAnywhere, BluePrintReadWrite)
	float MovementForce;
	UPROPERTY(EditAnywhere, BluePrintReadWrite)
	float JumpImpulse;
	UFUNCTION(BlueprintImplementableEvent)
	void Jump();

	UFUNCTION(Server,Reliable,WithValidation)
	void MoveUp(float Value);
	UFUNCTION(Server, Reliable, WithValidation)
	void MoveRight(float Value);
	UFUNCTION(Server, Reliable, WithValidation)
	void Resetti(int Value);
	UFUNCTION(Server, Reliable, WithValidation)
	void Fire();
	void Die(int ID);
};
