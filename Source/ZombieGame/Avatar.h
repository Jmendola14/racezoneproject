// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Avatar.generated.h"

UCLASS()
class ZOMBIEGAME_API AAvatar : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AAvatar();
	UPROPERTY()
	class ABallPlayer* Daddy;

	UPROPERTY(EditAnywhere, BluePrintReadWrite)
	bool isGrounded = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float dadSpeed = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float verticalSpeed = 0;
	int coolBuffer=0;
	UPROPERTY(EditAnywhere, BluePrintReadWrite)
	bool Jumped = false;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	

	void RotateToNormal(FVector dir);

};
