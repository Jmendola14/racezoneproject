// Fill out your copyright notice in the Description page of Project Settings.


#include "MisteryItem.h"
#include "BallPlayer.h"
// Sets default values
AMisteryItem::AMisteryItem()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AMisteryItem::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AMisteryItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AMisteryItem::NotifyActorBeginOverlap(AActor* OtherActor)
{
	ABallPlayer* other = dynamic_cast<ABallPlayer*>(OtherActor);
	if (other != nullptr)
	{
		other->ItemID = FMath::RandRange(1, 2);

		UE_LOG(LogTemp, Warning, TEXT("ItemIDRandom %d "), other->ItemID);
		
	}

}
