#include "Avatar.h"
#include "Kismet/KismetMathLibrary.h"
#include "DrawDebugHelpers.h"
#include "BallPlayer.h"

// Sets default values
AAvatar::AAvatar()
{
	PrimaryActorTick.bCanEverTick = true;

	SetReplicates(true);
	SetReplicateMovement(true);
}

// Called when the game starts or when spawned
void AAvatar::BeginPlay()
{
	PrimaryActorTick.TickGroup = TG_PostPhysics;
	Super::BeginPlay();
	
	
}
int buffer = 0;

void AAvatar::RotateToNormal(FVector dir)
{
	if (Daddy)
	{
		isGrounded = true;
		coolBuffer = 0;
		RootComponent->SetWorldRotation(FMath::Lerp(RootComponent->GetComponentQuat(), UKismetMathLibrary::FindLookAtRotation(RootComponent->GetComponentLocation(), dir).Quaternion(),1.0f), false);
		RootComponent->AddLocalRotation(FQuat(FVector(0,-1,0),3.14f/2.0f));

		FVector dadVelocity = Daddy->GetVelocity();
		
		RootComponent->SetWorldRotation(FQuat(RootComponent->GetComponentQuat().X, RootComponent->GetComponentQuat().Y, 0, RootComponent->GetComponentQuat().W));
		
		FQuat debugboi = (FVector(dadVelocity.X, dadVelocity.Y, 0) - FVector(0, 0, 0)).Rotation().Quaternion();


		
		
		RootComponent->AddLocalRotation(debugboi,false);
		

		

		DrawDebugLine(GetWorld(), RootComponent->GetComponentLocation(), dir, FColor().Green, false, 1);

		dadSpeed = dadVelocity.Size();
		verticalSpeed = dadVelocity.Z;

	}

}

// Called every frame
void AAvatar::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	buffer++;
	/*if (!ensure(Daddy))
	{
		return;
	}
	*/
	coolBuffer++;
	if (Daddy)
	{
		SetActorLocation(Daddy->GetActorLocation(), true);
		FVector dadVelocity = Daddy->GetVelocity();
		if (isGrounded == false)
		{
			//RootComponent->SetWorldRotation(FMath::Lerp(RootComponent->GetComponentQuat(),
			//(FVector(dadVelocity.X, dadVelocity.Y, 0) - FVector(0, 0, 0)).Rotation().Quaternion(), 0.05f), 0);
			RootComponent->SetWorldRotation(FMath::Lerp(RootComponent->GetComponentQuat(),
				(FVector(dadVelocity.X, dadVelocity.Y, dadVelocity.Z) - FVector(0, 0, 200)).Rotation().Quaternion(), 0.05f), 0);
		}

		if(coolBuffer>=7)
			isGrounded = false;


		dadSpeed = dadVelocity.Size();
		verticalSpeed = dadVelocity.Z;

		Jumped = Daddy->Jumped;
		//UE_LOG(LogTemp, Warning, TEXT("%f"), verticalSpeed);
	}


}

// Called to bind functionality to input
void AAvatar::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

