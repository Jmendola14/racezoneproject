// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/Button.h"
#include "MyButton.generated.h"

UCLASS()
class ZOMBIEGAME_API UMyButton : public UButton
{
	GENERATED_BODY()

		UMyButton();

	UFUNCTION()
		void OnClick();
public:
	void SetSessionInformation(struct FSessionInfo* sInfo);

	struct FSessionInfo* sessionInfo;
};
