// Fill out your copyright notice in the Description page of Project Settings.

#include "Missile.h"
#include "BallPlayer.h"
#include "ZombieGame.h"

//float Spaad = 0.f;

AMissile::AMissile(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	
	PrimaryActorTick.bCanEverTick = true;

	CollisionComp = ObjectInitializer.CreateDefaultSubobject<UBoxComponent>(this, TEXT("CollisionComp"));
	RootComponent = CollisionComp;


	
	MissileMesh = ObjectInitializer.CreateDefaultSubobject<UStaticMeshComponent>(this, TEXT("MissileMesh"));
	const ConstructorHelpers::FObjectFinder<UStaticMesh> MeshObj(TEXT("/Game/Missile/Missiles_Pack_Missil_03"));
	MissileMesh->SetStaticMesh(MeshObj.Object);
	MissileMesh->SetupAttachment(RootComponent);

	//  Movement Component
	ProjectileMovement = ObjectInitializer.CreateDefaultSubobject<UProjectileMovementComponent>(this, TEXT("ProjectileMovement"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 4500.f;
	ProjectileMovement->MaxSpeed = 2000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = false;
	ProjectileMovement->bInitialVelocityInLocalSpace = true;
	ProjectileMovement->bIsHomingProjectile = true;
	ProjectileMovement->HomingAccelerationMagnitude = 400.f;
	ProjectileMovement->ProjectileGravityScale = 0.f;
	ProjectileMovement->Velocity = FVector(0, 0, 0);
	//Spaad = ProjectileMovement->InitialSpeed;

	
	CollisionComp->OnComponentBeginOverlap.AddDynamic(this, &AMissile::OnOverlapBegin);

	// Set Default 
	hasTargetPosition = false;
	hasNoTarget = false;
	target = NULL;
	delayTimer = 0.f;
	hasFinishedDelay = false;
	lifetimeCountdown = 45.f;
	canBeDestroyed = false;
	//PlayerInWorld = NULL;

	SetReplicates(true);
	SetReplicateMovement(true);
}

#pragma region Setup Target Logic

void AMissile::BeginPlay()
{
	Super::BeginPlay();
	FindPlayer();


	wasPick = false;
	//  the Missile upwards
	if (!hasTargetPosition)
	{
		ProjectileMovement->Velocity = GetActorUpVector() * 200.f;

		this->SetActorEnableCollision(false);
	}

}


void AMissile::FindPlayer()
{
	FVector WantedDir = trgt->GetActorLocation();
	
}


void AMissile::UpdateTarget()
{
	if (!hasTargetPosition)
	{
		if (trgt != NULL)
		{
			if (trgt->IsValidLowLevel())
			{
				
				
				hasTargetPosition = true;
				hasNoTarget = false;


				FRotator rotVal = MissileMesh->GetComponentRotation();
				rotVal.Roll = 0.f;
				rotVal.Pitch = -90.f;
				rotVal.Yaw = 0.f;
				MissileMesh->SetRelativeRotation(rotVal);
			}
			else
			{
				trgt = nullptr;
				hasTargetPosition = true;
				hasNoTarget = true;
			}
		}
		else
		{
			trgt = nullptr;
			hasTargetPosition = true;
			hasNoTarget = true;
		}
	}
}


void AMissile::DelayLogic(float dTime)
{
	if (!hasFinishedDelay)
	{
		delayTimer += 1 * dTime;

		if (delayTimer > 1.f)
		{
			UpdateTarget();
			this->SetActorEnableCollision(true);
			hasFinishedDelay = true;
		}
	}
}
#pragma endregion



void AMissile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!hasFinishedDelay)
	{
		DelayLogic(DeltaTime);
	}
	else
	{
		// If a target is found, move the missile actor toward target
		if (hasTargetPosition)
		{
			if (trgt != NULL)
			{
				if (trgt->IsValidLowLevel())
				{
					//FVector wantedDir = (trgt->GetActorLocation() - GetActorLocation()).GetSafeNormal();
					//wantedDir += trgt->GetVelocity() * wantedDir.Size() / 200.f;
					//ProjectileMovement->Velocity += wantedDir * 400.f * DeltaTime;
					//ProjectileMovement->Velocity = ProjectileMovement->Velocity.GetSafeNormal() * 200.f;
					FVector WantedDir = (trgt->GetActorLocation() - GetActorLocation()).GetSafeNormal();
					ProjectileMovement->Velocity = WantedDir * ProjectileMovement->InitialSpeed;
					//ProjectileMovement->Velocity = WantedDir * ProjectileMovement->HomingAccelerationMagnitude;
				}
				else
				{
					if (!this->IsPendingKill())
						if (this->IsValidLowLevel())
							K2_DestroyActor();
				}
			}
			else
			{
				// If a target is NOT found, continue to move the missile actor upwards
				if (hasNoTarget)
				{
					ProjectileMovement->Velocity = GetActorUpVector() * 200.f;
					hasNoTarget = false;
				}
			}
		}

		// Destroy the missile actor after  countdown
		lifetimeCountdown -= 1 * DeltaTime;

		if (lifetimeCountdown < 0.f)
		{
			if (!canBeDestroyed)
			{
				canBeDestroyed = true;
				trgt = nullptr;
				Explode();
			}
		}
	}
}



#pragma region Overlap Events

void AMissile::OnOverlapBegin(UPrimitiveComponent* overlappedComp, AActor* otherActor, UPrimitiveComponent* otherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& hitResult)
{
	class ABallPlayer* PlayerCharacter = Cast<ABallPlayer>(otherActor);
	class AStaticMeshActor* GroundActor = Cast<AStaticMeshActor>(otherActor);

	if (PlayerCharacter != nullptr)
	{
		PlayExplosion(ExplosionSystem);
		PlayExplosionSound(ExplosionSound);

		if (this->IsValidLowLevel())
			Destroy();
	}

	/*if (GroundActor != nullptr)
	{
		PlayExplosion(ExplosionSystem);
		PlayExplosionSound(ExplosionSound);

		if (this->IsValidLowLevel())
			Destroy();
	}*/
}
#pragma endregion

#pragma region End of Play Logic
void AMissile::Explode()
{
	PlayExplosion(ExplosionSystem);
	PlayExplosionSound(ExplosionSound);

	if (this->IsValidLowLevel())
		Destroy();
}

// Spawn our explosion particle system
class UParticleSystemComponent* AMissile::PlayExplosion(class UParticleSystem* explosion)
{
	class UParticleSystemComponent* retVal = NULL;

	if (explosion)
	{
		class UWorld* const world = GetWorld();

		if (world)
		{
			FVector myPos = GetActorLocation();
			FRotator myRot = GetActorRotation();

			retVal = UGameplayStatics::SpawnEmitterAtLocation(world, explosion, myPos, myRot, true);
		}
	}

	return retVal;
}

// Spawn our explosion sound
class UAudioComponent* AMissile::PlayExplosionSound(class USoundCue* sound)
{
	class UAudioComponent* retVal = NULL;

	if (sound)
		retVal = UGameplayStatics::SpawnSoundAttached(sound, this->GetRootComponent());

	return retVal;
}
#pragma endregion

