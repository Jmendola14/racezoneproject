// Fill out your copyright notice in the Description page of Project Settings.


#include "MyLevelScriptActor.h"
#include "TimerManager.h"
#include "Components/Button.h"
#include "MyButton.h"
#include "Components/VerticalBox.h"
#include "Components/VerticalBoxSlot.h"
#include "Components/TextBlock.h"
void AMyLevelScriptActor::BeginPlay()
{
	Super::BeginPlay();
	readyToHost = false;
	serversList = new TArray<FSessionInfo*>();

	GetWorld()->GetTimerManager().SetTimer(serverListTimerHandle,
		this, &AMyLevelScriptActor::OnUpdateServerList, 2.f, true);

	UE_LOG(LogTemp, Warning, TEXT("Got Here 1"));
	if (MatchmakingWidgetClass)
	{
		UE_LOG(LogTemp, Warning, TEXT("Got Here 2"));
		UE_LOG(LogTemp, Warning, TEXT("2 Players Pog"));
		MatchmakingWidget = CreateWidget<UUserWidget>(GetWorld(), MatchmakingWidgetClass);
		MatchmakingWidget->AddToViewport();

		UButton* connectButton = Cast<UButton>(MatchmakingWidget->GetWidgetFromName(TEXT("ConnectButton")));
		if (connectButton)
		{

			connectButton->OnClicked.AddDynamic(this, &AMyLevelScriptActor::OnConnectClicked);

		}

		UButton* hostButton = Cast<UButton>(MatchmakingWidget->GetWidgetFromName(TEXT("HostButton")));
		if (hostButton)
		{

			hostButton->SetIsEnabled(false);
			hostButton->OnClicked.AddDynamic(this, &AMyLevelScriptActor::OnHostClick);

		}
		serverListScrollBox = Cast<UScrollBox>(MatchmakingWidget->GetWidgetFromName(TEXT("MyScrollBox")));
		APlayerController* pController = GetWorld()->GetFirstPlayerController();
		if (pController)
		{
			pController->bShowMouseCursor = true;
			pController->bEnableClickEvents = true;
			pController->bEnableMouseOverEvents = true;
		}
	}
}

void AMyLevelScriptActor::OnUpdateServerList()
{

	if (tcpClient)
	{

		if (tcpClient->IsConnected())
		{

			UButton* connectButton = Cast<UButton>(MatchmakingWidget->GetWidgetFromName(TEXT("ConnectButton")));
			if (connectButton)
			{
				connectButton->SetIsEnabled(false);

			}

			UButton* hostButton = Cast<UButton>(MatchmakingWidget->GetWidgetFromName(TEXT("HostButton")));
			if (connectButton)
			{
				hostButton->SetIsEnabled(true);

			}
			tcpClient->RequestServerList();
			if (readyToHost)
			{
				APlayerController* pController = GetWorld()->GetFirstPlayerController();
				if (pController)
				{
					pController->ConsoleCommand("open PedroMap?Listen");
				}
			}
		}


		if (serversList->Num() >= 0)
		{
			UE_LOG(LogTemp, Warning, TEXT("Message 660!!"));

			if ((MatchmakingWidget) && (serverListScrollBox))
			{
				UE_LOG(LogTemp, Warning, TEXT("Message 661!!"));
				TArray<UWidget*> allchildren = serverListScrollBox->GetAllChildren();

				for (int i = 0; i < allchildren.Num(); i++)
				{
					UE_LOG(LogTemp, Warning, TEXT("Message 663!!"));
					allchildren[i]->RemoveFromParent();

				}
				for (int i = 0; i < serversList->Num(); i++)
				{
					UE_LOG(LogTemp, Warning, TEXT("Message 664!!"));

					UVerticalBox* itemWidgetBox = NewObject<UVerticalBox>();
					serverListScrollBox->AddChild(itemWidgetBox);
					UMyButton* itemWidget = NewObject <UMyButton>(this);
					itemWidget->SetSessionInformation((*serversList)[i]);
					UTextBlock* itemWidgetText = NewObject <UTextBlock>();
					itemWidgetText->SetText(FText::FromString((*serversList)[i]->name));
					itemWidget->AddChild(itemWidgetText);
					UVerticalBoxSlot* slot = itemWidgetBox->AddChildToVerticalBox(itemWidget);
					static FMargin Padding(5);
					slot->SetPadding(Padding);
				}
			}
		}
	}
}

void AMyLevelScriptActor::OnConnectClicked()
{
	tcpClient = new TCPClient(this);

}

void AMyLevelScriptActor::OnHostClick()
{
	tcpClient->HostNewGame("My test server", "7777");
}

void AMyLevelScriptActor::UpdateSessionList(FString serverinfo)
{
	TArray<FString> Out;
	serverinfo.ParseIntoArray(Out, TEXT("|"), true);
	for (int i = 1; i < Out.Num() - 3; i += 4)
	{
		FSessionInfo* sInfo = new FSessionInfo();
		sInfo->id = FCString::Atoi(*Out[i]);
		sInfo->name = Out[i + 1];
		sInfo->serverip = Out[i + 2];
		sInfo->serverport = FCString::Atoi(*Out[i + 3]);
		bool sExist = false;
		
		

		for (int j = 0; j < serversList->Num(); j++)
		{
			if ((*serversList)[j]->id == sInfo->id)
			{
				sExist = true;
			}
		}
		if (!sExist)
		{
			serversList->Add(sInfo);
		}


	}
}

void AMyLevelScriptActor::StartGameHost(int port)
{
	HostpPort = port;
	readyToHost = true;

}