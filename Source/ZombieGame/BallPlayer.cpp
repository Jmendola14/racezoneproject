// Fill out your copyright notice in the Description page of Project Settings.


#include "BallPlayer.h"
#include <cmath>
#include <iostream>
#include "Laps.h"
#include "Kismet/GameplayStatics.h"

#include "Missile.h"

bool WasRight = false;

// Sets default values
ABallPlayer::ABallPlayer()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	SpringArm = CreateDefaultSubobject<USpringArmComponent>("SpringArm");
	Camera = CreateDefaultSubobject<UCameraComponent>("Camera");
	RootComponent = Mesh;
	SpringArm->SetupAttachment(Mesh);
	Camera->SetupAttachment(SpringArm);

//	VelocityHelper= FVector(0,0,0);

	Mesh->SetSimulatePhysics(true);
	MovementForce = 140000;
	
	
	SetReplicates(true);
	SetReplicateMovement(true);

	

}

TArray<AActor*> foundObjects;
TSubclassOf<ALaps> classToFind;
UMyGameInstance* GI;
TArray<AActor*> foundPlayers;
TSubclassOf<ABallPlayer> classPlayers;
FVector cameraLean;
float leanFwd=0, leanRght=0;
// Called when the game starts or when spawned
void ABallPlayer::BeginPlay()
{
	Super::BeginPlay();

	classToFind = ALaps::StaticClass();
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), classToFind, foundObjects);
	classPlayers = ABallPlayer::StaticClass();
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), classPlayers, foundPlayers);
	
	Mesh->OnComponentHit.AddDynamic(this, &ABallPlayer::OnHit);
	Spawn();
	timer = 0;
	UE_LOG(LogTemp, Warning, TEXT("Got Here 2"));
	cameraLean = FVector().ZeroVector;
    //thisAvatar = SpawnActor<AAvatar>(Mesh->GetLocation(), Mesh->GetRotation(), NULL, Instigator, true);
}
UWorld* thisworld;
void ABallPlayer::Spawn()
{
	thisworld = this->GetWorld();
	
	

	GI = Cast<UMyGameInstance>(GetGameInstance());
	if (GI)
	{
		
		GI->numPlayers++;
		currentID = GI->numPlayers;
		GI->addMeToInstance(currentID);
	}
	Die(0);
	if (ToSpawn)
	{
		UWorld* world = GetWorld();
		if (world)
		{
			FActorSpawnParameters spawnParams;
			spawnParams.Owner = this;

			FRotator rotator;

			FVector spawnLocation = Mesh->GetComponentLocation();

			AAvatar* Avatar = world->SpawnActor<AAvatar>(ToSpawn, spawnLocation, rotator, spawnParams);
			thisAvatar = Avatar;
			Avatar->Daddy = Cast<ABallPlayer>(this);
		}
	}

}

float ABallPlayer::GetTimer()
{
	return timer;
}
bool ABallPlayer::Won()
{
	if(currentLap == 3)
	{
		return true;
	}
	return false;
}

bool ABallPlayer::ResetEverything()
{
	Resetti(0);
	return true;
}

int ABallPlayer::GetLaps()
{
	return currentLap;
}
int ABallPlayer::GetPlacement()
{

	return currentPlace;
}

float ABallPlayer::GetJumpDelay()
{
	return jumpTimer;
}
float ABallPlayer::SetJumpDelay()
{
	jumpTimer = 0;
	return jumpTimer;
}

void ABallPlayer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	SetActorLocation(GetActorLocation(), true);
	if(currentLap<3)
		timer += DeltaTime;

	jumpTimer += DeltaTime;
	//VelocityHelper = GetVelocity();
	//SpringArm->AddRelativeRotation(GetVelocity().Rotation(), true);



	//UE_LOG(LogTemp, Warning, TEXT("MyID is %d"), currentID);
	if (GI)
	{
		if(GetWorld()->GetName()!="OfflinePedroMap")
			if (GI->numPlayers <= 2)
			{
			//UE_LOG(LogTemp, Warning, TEXT("2 Players Pog"));
				timer = 0;
				Die(1);
			}

	}
	if (abs(GetVelocity().X) > 4 || abs(GetVelocity().Z) >4)
	{
		//SpringArm->SetWorldRotation(GetVelocity().Rotation().Quaternion(),true);
		cameraLean = FVector(0, (leanRght*200)*-1, (leanFwd*200));
		SpringArm->SetWorldRotation(FMath::Lerp(SpringArm->GetComponentQuat(),
			((GetVelocity() + cameraLean) - FVector(0,0,200)).Rotation().Quaternion(), 0.05f), 0);
		
		//SpringArm->AddLocalRotation(cameraLean.Rotation().Quaternion(), false);

	}
	
}

// Called to bind functionality to input
void ABallPlayer::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MoveForward", this, &ABallPlayer::MoveUp);
	PlayerInputComponent->BindAxis("MoveRight", this, &ABallPlayer::MoveRight);
	PlayerInputComponent->BindAxis("LookUp", this, &ABallPlayer::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("Turn", this, &ABallPlayer::AddControllerYawInput);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ABallPlayer::Jump);
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ABallPlayer::Fire);

}
void ABallPlayer::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	
	GLog->Log(*OtherActor->GetName());
	Jumped = false;
	thisAvatar->RotateToNormal(Hit.ImpactPoint);
	

}
void ABallPlayer::Resetti_Implementation(int value)
{
	TArray<AActor*> toReset;
	UGameplayStatics::GetAllActorsOfClass(thisworld, ABallPlayer::StaticClass(), toReset);
	UE_LOG(LogTemp, Warning, TEXT("Got in resetti"));
	for (AActor* actor : toReset)
	{
		ABallPlayer* plr = Cast<ABallPlayer>(actor);
		if (plr != nullptr)
		{
			plr->timer = 0;
			plr->currentLap = 0;
			plr->currentLapID = 0;
			plr->Die(0);
		}
	}
}
bool ABallPlayer::Resetti_Validate(int value)
{
	return true;
}

void ABallPlayer::MoveUp_Implementation(float value)
{
	FVector DirectionHelp = Camera->GetForwardVector();
	FVector ForceToAdd = DirectionHelp * MovementForce * value;

	leanFwd = value;
	Mesh->AddForce(ForceToAdd);
		
	
}
bool ABallPlayer::MoveUp_Validate(float value)
{
	return true;
}

void ABallPlayer::MoveRight_Implementation(float value)
{
	FVector DirectionHelp = Camera->GetRightVector();
	FVector Aux = GetVelocity();

	leanRght = value;
	FVector ForceToAdd =  DirectionHelp * MovementForce * value*1.5f;
	Mesh->AddForce(ForceToAdd);
}
bool ABallPlayer::MoveRight_Validate(float value)
{
	return true;
}
void ABallPlayer::Die(int ID)
{
	
	Mesh->SetSimulatePhysics(false);
	Mesh->SetSimulatePhysics(true);
	if(ID==0)
	{ 
		
		for (int idx = 0; idx < foundObjects.Num(); idx++)
		{
			ALaps* Lap = Cast<ALaps>(foundObjects[idx]);
			if (Lap->CheckpointID == currentLapID)
				SetActorLocation(Lap->GetActorLocation(), false);

		}
	}
	if (ID == 1)
		timer = 0;
	
}


void ABallPlayer::Fire_Implementation()
{
	UE_LOG(LogTemp, Warning, TEXT("ItemID %d "), ItemID);
	
	switch (ItemID)
	{
	case 0:
	{


		break;
	}

	case 1:
	{
		//Missile

		//if (MissileClass != NULL)
		//{
			class UWorld* const world = GetWorld();


			for (int idx = 0; idx < foundPlayers.Num(); idx++)
			{
				ABallPlayer* plr = Cast<ABallPlayer>(foundPlayers[idx]);
				if (plr != this)
				{
					if (world != NULL)
					{
						FVector currentPos = GetActorLocation();
						FRotator currentRot = FRotator(0, 0, 0);

						FActorSpawnParameters spawnParams;
						spawnParams.Owner = this;
						spawnParams.Instigator = Instigator;

						class AMissile* FiredMissile = world->SpawnActor<AMissile>(MissileClass, currentPos, currentRot, spawnParams);
						FiredMissile->trgt = plr;
						if (FiredMissile != nullptr)
						{
							// Set Mesh Rotation Offset. This rotation is based upon how your missile FBX was modeled.
							FRotator meshRot = FiredMissile->MissileMesh->GetComponentRotation();
							meshRot.Roll = 0.f;
							meshRot.Pitch = -90.f;
							meshRot.Yaw = 0.f;
							FiredMissile->MissileMesh->SetRelativeRotation(meshRot);
						}
					}
				}

			}
		//}
		ItemID = 0;
		break;
	}

	case 2:
	{
		//SpeedBoost
		FVector aux = GetVelocity();
		FVector DirectionHelp = Camera->GetForwardVector();
		FVector ForceBoost = FVector(DirectionHelp.X, 0, DirectionHelp.Z) * 70000000;// GetVelocity() * 2;

		Mesh->AddForce(ForceBoost);
		UE_LOG(LogTemp, Warning, TEXT("THIS BALL IS BAD!!!!!"));
		ItemID = 0;
		break;
	}
	case 3:
	{
		//Shield

		ItemID = 0;
		break;
	}

	break;
	}

}
bool ABallPlayer::Fire_Validate()
{
	return true;
}




