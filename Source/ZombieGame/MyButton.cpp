// Fill out your copyright notice in the Description page of Project Settings.


#include "MyButton.h"
#include "MyLevelScriptActor.h"

UMyButton::UMyButton()
{
	OnClicked.AddDynamic(this, &UMyButton::OnClick);
}
void UMyButton::OnClick()
{
	APlayerController* pController = GetOuter()->GetWorld()->GetFirstPlayerController();
	if (pController)
	{
		UE_LOG(LogTemp, Warning, TEXT("Message sent!!"));
		FString cmd = "open " + this->sessionInfo->serverip + ":" + FString::FromInt(this->sessionInfo->serverport);
		pController->ConsoleCommand(cmd);
	}

}
void UMyButton::SetSessionInformation(FSessionInfo* sInfo)
{
	sessionInfo = sInfo;
}
